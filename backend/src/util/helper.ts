import * as mysql from "promise-mysql";
import * as fs from 'fs';
import * as mail from 'nodemailer';
import * as jimp from 'jimp';
import * as jwt from 'jsonwebtoken';
import { json } from "body-parser";
import { TResponse, ResponseCode, ResponseMessage } from '../util/TResponse';


const AWS = require("aws-sdk");
const ses = new AWS.SES({ apiVersion: "2010-12-01" });

AWS.config.update({
    accessKeyId: 'AKIA4FEXM4SUQJ4XQGXB',
    secretAccessKey: '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',
    region: 'ap-southeast-2'
});
export function to(promise) {
  return promise
    .then(data => {
      return [null, data];
    })
    .catch(err => [err]);
}
export function getError(actualError: Error, showError: string = "") {
  console.log("#########Error", actualError);
  if (showError.length > 0) {
    if (actualError['IsHandled'] != true) {
      let err: Error = actualError;
      err.message = showError;
      actualError['IsHandled'] = true;
      return err;
    }
  }
  return actualError;
}
export function promiseWrapper(callback) {
  return new Promise((acc, rej) => {
    try {
      // console.log('@@@@@@@@@@@@',callback);
      return acc(callback());
    } catch (e) {
      return Promise.reject(e);
    }
  }).catch(err => {
    return Promise.reject(err);
  });
}
export function isValueExist(param): boolean {
  if (param) {
    if (param != undefined && param != null) {
      if (typeof param == "string") {
        if (param.length > 0) {
          return true;
        } else {
          return false;
        }
      } else if (param instanceof Array) {
        if (param.length > 0) {
          return true;
        } else {
          return false;
        }
      } else {
        return true;
      }
    }
    return false;
  }
  return false;
}
export async function readFile(path): Promise<any> {
  return new Promise(async (acc, rej) => {
    fs.readFile(path, function (err, data) {
      if (err) {
        return rej(getError(err));
      }
      return acc(data);
    });
  }).catch(err => {
    return Promise.reject(err);
  });
}
// export function invokeLambda(params: any) {
//   console.log("data", params);
//   let AWS = require('aws-sdk');
//   let lambda = new AWS.Lambda();
//   lambda.invoke(params, function (err, data) {
//     if (err) {
//       console.log('invokeLambda err...................below...');
//       console.log(err);
//     } else {
//       console.log('invokeLambda success...................below...');
//       console.log("data.Payload", data.Payload);
//     }
//   });
// }


// export function invokeBackgroundTask(data: any, methodName: any) {
//   console.log("methodName", methodName);
//   let payload: any = { data: data, methodName: methodName };
//   var lparams = {
//     FunctionName: process.env['SELF_SERVICE_NAME'] + 'Notification',
//     InvocationType: 'Event',
//     LogType: 'Tail',
//     Payload: `{"body" : ${JSON.stringify(payload)}}`
//   };

//   console.log(`params invoke===== ${JSON.stringify(payload)}`);
//   console.log("Waiting");

//   invokeLambda(lparams);
// }
export function getTokenData(event: any): any {
  let authUser: any = {};
  try {
    let token = event.headers.Authorization || null;
    const decoded = jwt.verify(token, process.env.EncryptionKEY);
    authUser = decoded.data; //session jwt user
  } catch (e) {
    console.log("error in token")
  }
  return authUser;
}
export async function EmailSend(param: any): Promise<any> {
  return new Promise(async (acc, rej) => {
    const sendEmail = ses.sendEmail(param).promise();
    sendEmail.then((data:any) => {
        console.log("email submitted to SES", data);
    })
    .catch((error:any) => {
        console.log(error);
    });
    
  }).catch((err) => {
    console.log('>>>>>>>Error in mail');
    return Promise.reject(err);
  });
}
export const getPassword = () => {
  var pass = ''; 
  var str = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'+'abcdefghijklmnopqrstuvwxyz0123456789@#$'; 
  for (let i = 1; i <= 8; i++) { 
      var char = Math.floor(Math.random() 
                  * str.length + 1); 
        
      pass += str.charAt(char) 
  } 
    
  return pass; 
} 
export const UUIDV4 = () => {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
    var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
    return v.toString(16);
  });
}
export async function authorize(event: any) {
  try {
      const token = event.headers.Authorization;
      let user: any = null;
      // then its customer which has public access
      if (token) {
          const decoded = jwt.verify(token, process.env.EncryptionKEY);
          user = decoded.data;
      } 
      
      if (!user) {
          let err: any = new Error("Your are not allowed to access this resource");
          // err.code = ResponseCode.NOT_ACCEPTABLE;
          throw err;
      }
  } catch (e) {
      if (!e.code)
          e.code = ResponseCode.UNAUTHORIZED;
      throw e; // Return a 401 Unauthorized response
  }
}

// export function authorizeUser(userScopes: any, methodArn: any): any {
//   //const hasValidScope = _.some(userScopes, scope => _.endsWith(methodArn, scope));
//   return hasValidScope;
// };

export function buildIAMPolicy(userId, effect, resource, context): any {
  const policy = {
      principalId: userId,
      policyDocument: {
          Version: '2012-10-17',
          Statement: [
              {
                  Action: 'execute-api:Invoke',
                  Effect: effect,
                  Resource: "*",
              },
          ],
      },
      context,
  };

  return policy;
};