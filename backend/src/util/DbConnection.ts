import * as mysql from "promise-mysql";
import { to } from "./helper";

export namespace DbConnection {
  export var connection;

  export async function getConnection() {
    if (!connection) {
      let [err, con] = await to(createConnection());
      connection = con;
    }
    return connection;
  }

  export async function disposeConnection() {
    if (connection) {
      connection.end();
      connection = null;
    }
  }

  function createConnection(): Promise<any> {
    return new Promise(async (acc, rej) => {
      let connection = await mysql.createConnection({
        host: process.env.MYSQL_HOST,
        user: process.env.MYSQL_USER,
        password: process.env.MYSQL_PASSWORD,
        database: process.env.MYSQL_DB,
        typeCast: function castField(field, useDefaultTypeCasting) {
          if (field.type === "BIT" && field.length === 1) {
            var bytes = field.buffer();
            return bytes[0] === 1;
          }
          return useDefaultTypeCasting();
        },
        multipleStatements: true
      });

      return acc(connection);
    }).catch(err => {
      return Promise.reject(err);
    });
  }
}
