export class TResponse {
    ResponseCode: number = ResponseCode.Ok;
    ResponseMessage: string = "";
    ResponseData?: any;
}
export enum ResponseCode {
    Ok = 200,
    Error = 201,
    AlreadyLogin = 202,
    UNAUTHORIZED = 401,
    NOT_ACCEPTABLE = 406,
    METHOD_NOT_ALLOWED = 405,
    FORBIDDEN = 403,
}
export enum MethodKeys {
    EmailSend = 'EmailSend',
    mailtoCustomer = 'mailtoCustomer',
    mailtoTaxi = 'mailtoTaxi',
}
export const UserRoles = {
    Customers: ["userLogin","forgotPassword", "resetPassword"]
}
export enum ResponseMessage {
    success = "Success",
    error = "Error",
    notFound = "No record found",
    fetch_data_error = "Error in fetching data",
    recordSavedSuccess = "Record saved Successfully",
    recordSavedError = "Error in saving record",
    invalid_email_Password = "Invalid email or password",
    user_not_exist = "User doesn't exists",
    user_not_active = "User is inactive",
    email_already_exist = "Email ID already exits. Please login",
    user_created_success = "User added successfully",
    user_update_success = "User updated successfully",
    user_save_error = "Error in saving user",
    login_error = "Error in login user",
    password_changed_success = "Password changed successfully",
    password_change_error = "Error in changing password",
    old_password_not_match = "Old password is incorrect",
    reset_password_sent_success = 'Instructions has been sent on your email address to reset password',
    reset_password_sent_error = 'Error in sending reset password link',
    reset_password_token_expire = 'Password link expire',
    old_password_same_as_new = "Old password is same as new password",
    reset_pass_unauthorized = 'Unauthorized user.',
    LINK_EXPIRED = 'The password reset link is no longer valid. Please request another password reset email from the login page.'
}