export class ErrorHandling extends Error {

    public IsHandled: boolean = false;

    constructor(customMessage: string = "") {
        super();
        if (customMessage != "") {
            if (!this.IsHandled) {
                this.message = customMessage;
                this.IsHandled = true;
            }
        }
    }

    public setError(customMessage: string = "") {
        console.log("@@@@@@Error", this);
        if (customMessage != "") {
            if (!this.IsHandled) {
                this.message = customMessage;
                this.IsHandled = true;
            }
        }
    }

    public getError(): ErrorHandling {
        console.log("@@@@@@Error", this);
        return this;
    }
}