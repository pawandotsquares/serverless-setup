import { UserModel } from '../Model/UserModel';
import { TResponse, ResponseCode, ResponseMessage, MethodKeys, UserRoles } from '../util/TResponse';
import { to, isValueExist, getError, readFile, EmailSend, getPassword, UUIDV4 } from '../util/helper';
import * as dotenv from 'dotenv';
import * as _ from 'lodash';
import * as CryptoJS from 'crypto-js';
import { DbConnection } from '../util/DbConnection';
import { Guid } from "guid-typescript";
import * as jwt from 'jsonwebtoken';

export class UserBusiness {
    response: TResponse;
    constructor() {
        dotenv.config();
    }

    async userLogin(param: any): Promise<any> {

        return new Promise(async (acc, rej) => {
            let data = param;
            let err: Error;
            let connection;
            [err, connection] = await to(DbConnection.getConnection());
            if (err) {
                return rej(getError(err));
            }
            let result: any;
            
            var query = `SELECT * FROM user where email = ? and password= ?;`;
            //console.log()
            [err, result] = await to(connection.query(query, [data.email, CryptoJS.SHA512(data.password, process.env.EncryptionKEY).toString()]));
            if (err) {
                return rej(getError(err));
            }
            
            if (result && result.length > 0) {
                let user = new UserModel(result[0]);

                let token = jwt.sign({
                    data: user
                }, process.env.EncryptionKEY, { expiresIn: '7d' });

                user['token'] = token;

                if (user.is_deleted == true) {
                    this.response = {
                        ResponseCode: ResponseCode.Error,
                        ResponseMessage: ResponseMessage.user_not_exist
                    }
                }
                else if (user.is_active == false) {
                    this.response = {
                        ResponseCode: ResponseCode.Error,
                        ResponseMessage: ResponseMessage.user_not_active
                    }
                }
                else {
                    this.response = {
                        ResponseCode: ResponseCode.Ok,
                        ResponseMessage: ResponseMessage.success,
                        ResponseData: user
                    }
                }
            }
            else {
                this.response = {
                    ResponseCode: ResponseCode.Error,
                    ResponseMessage: ResponseMessage.invalid_email_Password
                }
            }
            return acc(this.response);
        }).catch((err) => {
            return Promise.reject(err);
        });
    }

    // async getEmail(param: any): Promise<any> {

    //     return new Promise(async (acc, rej) => {

    //         let data = param;
    //         let err: Error;
    //         let connection;
    //         [err, connection] = await to(DbConnection.getConnection());
    //         if (err) {
    //             return rej(getError(err));
    //         }
    //         let result: any;


    //         var query = `select email_id from user where id= ?;`;

    //         [err, result] = await to(connection.query(query, [param.id]));

    //         if (err) {
    //             return rej(getError(err));
    //         }

    //         if (result && result.length > 0) {
    //             let user = new UserModel(result[0]);

    //             if (user.is_deleted == true) {
    //                 this.response = {
    //                     ResponseCode: ResponseCode.Error,
    //                     ResponseMessage: ResponseMessage.user_not_exist
    //                 }
    //             }
    //             else {
    //                 this.response = {
    //                     ResponseCode: ResponseCode.Ok,
    //                     ResponseMessage: ResponseMessage.success,
    //                     ResponseData: user
    //                 }
    //             }
    //         }
    //         else {
    //             this.response = {
    //                 ResponseCode: ResponseCode.Error,
    //                 ResponseMessage: ResponseMessage.invalid_email_Password
    //             }
    //         }
    //         return acc(this.response);
    //     }).catch((err) => {
    //         return Promise.reject(err);
    //     });
    // }

    async forgotPassword(param: any): Promise<any> {
        return new Promise(async (acc, rej) => {
            console.log("test-------------","%J",param); 
            let data = param;
            let err: Error, result: any;
            let connection;
            [err, connection] = await to(DbConnection.getConnection());
            if (err) {
                return rej(getError(err));
            }
            let user: any;
            var query = '';
            query = `select id,user_name,email from user where email= ?;`;
            
            [err, user] = await to(connection.query(query, [data.email]));
            if (err) {
                return rej(getError(err));
            }
            
            if (isValueExist(user) && isValueExist(user[0].id)) {

                var random = UUIDV4();
                
                let query = `update user set auth_key = ? where email = ?;`;
                [err, result] = await to(connection.query(query, [random,data.email]));
               
               
                if(result){
                    const url = process.env.front_url+"/resetpassword/?token="+random;
                    //const url = 'www.google.com';
                    const params = {
                        Destination: {
                            ToAddresses: [user[0].email] // Email address/addresses that you want to send your email
                        },                   
                        Message: {
                            Body: {
                                Html: {
                                   
                                    Data:
                                    `<html>
                                        <body>
                                            <h1>Hello `+user.user_name+`</h1>
                                            <p>Please  <a href="`+url+`">click here</a> to reset your password</p>
                                            <p> If you are unable to click please use `+url+` url to reset password</p>
                                        </body>
                                    </html>`
                                }
                            },
                            Subject: {
                                Charset: "UTF-8",
                                Data: "Adelante: Forgot Password"
                            }
                        },
                        Source: "support@theadelantegroup.com"
                    };
                    const sendEmail = EmailSend(params);
                    
                   
                }
                this.response = {
                    ResponseCode: ResponseCode.Ok,
                    ResponseMessage: ResponseMessage.reset_password_sent_success
                }
            }
            else {
                this.response = {
                    ResponseCode: ResponseCode.FORBIDDEN,
                    ResponseMessage: ResponseMessage.user_not_exist
                }
            }
            return acc(this.response);
        }).catch((err) => {
            return Promise.reject(getError(err, ResponseMessage.reset_password_sent_error));
        });
    }

    async changePassword(param: any): Promise<any> {

        return new Promise(async (acc, rej) => {
            let data = param;
            let err: Error;
            let connection;
            [err, connection] = await to(DbConnection.getConnection());
            if (err) {
                return rej(getError(err));
            }

            let result: any;
            var query = `select password from user where id= ?;`;

            [err, result] = await to(connection.query(query, [data.id]));

            if (err) {
                return rej(getError(err));
            }
            if (isValueExist(result[0].password)) {
                let password = result[0].password;
                if (password == CryptoJS.SHA512(data.new_password, process.env.EncryptionKEY).toString()) {
                    this.response = {
                        ResponseCode: ResponseCode.Error,
                        ResponseMessage: ResponseMessage.old_password_same_as_new
                    }
                }
                else if (password == CryptoJS.SHA512(data.password, process.env.EncryptionKEY).toString()) {

                    query = `Update user
                                set password='`+ CryptoJS.SHA512(data.new_password, process.env.EncryptionKEY).toString() + `'
                                where id=`+ data.id + `;`;

                    [err, result] = await to(connection.query(query));

                    if (err) {
                        return rej(getError(err));
                    }

                    this.response = {
                        ResponseCode: ResponseCode.Ok,
                        ResponseMessage: ResponseMessage.password_changed_success
                    }
                }
                else {
                    this.response = {
                        ResponseCode: ResponseCode.Error,
                        ResponseMessage: ResponseMessage.old_password_not_match
                    }
                }

            }
            else {
                this.response = {
                    ResponseCode: ResponseCode.Error,
                    ResponseMessage: ResponseMessage.user_not_exist
                }
            }
            return acc(this.response);
        }).catch((err) => {
            return Promise.reject(err);
        });


    }

    // async adminDashboard(param: any): Promise<any> {

    //     return new Promise(async (acc, rej) => {
    //         let result: any;
    //         let err: Error;

    //         let connection;

    //         [err, connection] = await to(DbConnection.getConnection());

    //         if (err) {
    //             return rej(getError(err));
    //         }

    //         var query = `call GetAdminDashboardData();`;
    //         [err, result] = await to(connection.query(query));
    //         if (err) {
    //             return rej(getError(err));
    //         }
    //         console.log('############', result);

    //         this.response = {
    //             ResponseCode: ResponseCode.Ok,
    //             ResponseMessage: ResponseMessage.success,
    //             ResponseData: result[0]
    //         }

    //         return acc(this.response);
    //     }).catch((err) => {
    //         return Promise.reject(err);
    //     });
    // }
    async resetPassword(param: any): Promise<any> {
        return new Promise(async (acc, rej) => {
            let data = param;
            let err: Error;
            let connection;
            [err, connection] = await to(DbConnection.getConnection());
            if (err) {
                return rej(getError(err));
            }
            let result: any;
            var query = `SELECT * FROM user where auth_key = ?`;
            [err, result] = await to(connection.query(query, [param.token]));
            if (err) {
                return rej(getError(err));
            }
            if (result.length < 1) {
                this.response = {
                    ResponseCode: ResponseCode.Error,
                    ResponseMessage: ResponseMessage.LINK_EXPIRED
                }
                return acc(this.response);
            }else{
                
                let password = CryptoJS.SHA512(data.password, process.env.EncryptionKEY).toString()
                let resultUpdate: any;
                let query = `update user set auth_key = '', password = ? where auth_key = ?;`;
                [err, resultUpdate] = await to(connection.query(query,[password,data.token]));

                if (err) {
                    return rej(getError(err));
                }
                if(resultUpdate){
                    this.response = {
                        ResponseCode: ResponseCode.Ok,
                        ResponseMessage: ResponseMessage.password_changed_success
                    }
                    return acc(this.response);
                }
            }
        }).catch((err) => {
            return Promise.reject(err);
        });
    }

    async getUserByRole(param: any): Promise<any> {
        return new Promise(async (acc, rej) => {
            let data = param;
            let err: Error;
            let connection;
            [err, connection] = await to(DbConnection.getConnection());
            if (err) {
                return rej(getError(err));
            }
            let result: any;
            var query = `SELECT user.id,user.user_name,user.email,user.profile_image FROM user left join user_roles on user.id = user_roles.user_id WHERE user.is_active = 1 and user.is_deleted = 0 `;
            var dataArr:any = [];
            if(data.role > 0){
                query += `  and user_roles.role_id = ?`
                dataArr.push(data.role);
            }
            if(data.orgId > 0){
                query += ` and organization_id = ?`
                dataArr.push(data.orgId);
            }
            [err, result] = await to(connection.query(query, dataArr));
            if (err) {
                return rej(getError(err));
            }
            if (result) {
                this.response = {
                    ResponseCode: ResponseCode.Ok,
                    ResponseMessage: "",
                    ResponseData:result
                }
                return acc(this.response);
            }
        }).catch((err) => {
            return Promise.reject(err);
        });
    }
    async getUserRoles(param: any): Promise<any> {
        return new Promise(async (acc, rej) => {
            let data = param;
            let err: Error;
            let connection;
            [err, connection] = await to(DbConnection.getConnection());
            if (err) {
                return rej(getError(err));
            }
            let result: any;
            var query = `SELECT user.id,user.user_name,user.email,user.profile_image FROM user left join user_roles on user.id = user_roles.user_id WHERE user.is_active = 1 and user.is_deleted = 0 `;
            var dataArr:any = [];
            if(data.role > 0){
                query += `  and user_roles.role_id = ?`
                dataArr.push(data.role);
            }
            if(data.orgId > 0){
                query += ` and organization_id = ?`
                dataArr.push(data.orgId);
            }
            [err, result] = await to(connection.query(query, dataArr));
            if (err) {
                return rej(getError(err));
            }
            if (result) {
                this.response = {
                    ResponseCode: ResponseCode.Ok,
                    ResponseMessage: "",
                    ResponseData:result
                }
                return acc(this.response);
            }
        }).catch((err) => {
            return Promise.reject(err);
        });
    }


}