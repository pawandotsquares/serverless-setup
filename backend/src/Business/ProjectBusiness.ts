import { ProjectModel } from '../Model/ProjectModel';
import { TResponse, ResponseCode, ResponseMessage, MethodKeys, UserRoles } from '../util/TResponse';
import { to, isValueExist, getError, readFile, EmailSend, getPassword, UUIDV4 } from '../util/helper';
import * as dotenv from 'dotenv';
import * as _ from 'lodash';
import * as CryptoJS from 'crypto-js';
import { DbConnection } from '../util/DbConnection';
import { Guid } from "guid-typescript";
import * as jwt from 'jsonwebtoken';

export class ProjectBusiness {
    response: TResponse;
    constructor() {
        dotenv.config();
    }
    async addProject(param: any): Promise<any> {

        return new Promise(async (acc, rej) => {
           let data = param;
            let err: Error;
            let connection;
            [err, connection] = await to(DbConnection.getConnection());
            if (err) {
                return rej(getError(err));
            }
            let result: any;

            let date_ob = new Date();
            
            let query = `INSERT INTO project(title, owner_id, organisation_id, project_type, current_period, is_group,is_active,created_at,team_leader) VALUES (?,?,?,?,?,?,?,?,?)`;
            var dataArr = [data.projectName,data.projectOwner,data.organization,data.projectType,data.currentPeriod,data.group,data.projectActive,date_ob,data.teamLeader];
            
            //console.log()
            [err, result] = await to(connection.query(query, dataArr));
            if (err) {
                return rej(getError(err));
            }
            if (result && result.length > 0) {
                console.log(result);
                this.response = {
                    ResponseCode: ResponseCode.Ok,
                    ResponseMessage: ResponseMessage.success,
                    ResponseData: "",
                }
                return acc(this.response);
            }
            
        }).catch((err) => {
            return Promise.reject(err);
        });
    }
    async addProjectUser(param: any): Promise<any> {
        return new Promise(async (acc, rej) => {
           let data = param;
            let err: Error;
            let connection;
            [err, connection] = await to(DbConnection.getConnection());
            if (err) {
                return rej(getError(err));
            }
            let result: any;

            let date_ob = new Date();
            
            let query = `INSERT INTO project_user(user_id, project_id, role_id, created_at) VALUES (?,?,?,?)`;
            var dataArr = [data.selectedUserId,data.projectId,data.roleId,date_ob];
            
            //console.log()
            [err, result] = await to(connection.query(query, dataArr));
            if (err) {
                return rej(getError(err));
            }
            if (result && result.length > 0) {
                let queryUsers = `SELECT user.user_name,user.email,user.title,org.user_name as company_name,project_user.is_active
                FROM project_user 
                join user on project_user.user_id = user.id 
                left join user as org on user.organization_id = org.id
                WHERE project_user.project_id = ? and project_user.is_deleted = 0`
                var dataArr = [data.projectId];
                let resultUsers = await connection(queryUsers, dataArr);
                this.response = {
                    ResponseCode: ResponseCode.Ok,
                    ResponseMessage: ResponseMessage.success,
                    ResponseData: resultUsers,
                }
                return acc(this.response);
            }
            
        }).catch((err) => {
            return Promise.reject(err);
        });
    }
    async updateProjectUserStatus(param: any): Promise<any> {

        return new Promise(async (acc, rej) => {
            let data = param;
            let err: Error;
            let connection;
            [err, connection] = await to(DbConnection.getConnection());
            if (err) {
                return rej(getError(err));
            }
            let result: any;

            let query = `UPDATE project_user SET is_active = ? WHERE user_id = ? and project_id = ?`;
            var dataArr = [data.status,data.userId,data.projectId];
           
            [err, result] = await to(connection.query(query, dataArr));
            if (err) {
                return rej(getError(err));
            }
            if (result && result.length > 0) {
                console.log(result);
                this.response = {
                    ResponseCode: ResponseCode.Ok,
                    ResponseMessage: ResponseMessage.success,
                    ResponseData: "",
                }
                return acc(this.response);
            }
            
        }).catch((err) => {
            return Promise.reject(err);
        });
    }
    async getProjectUsers(param: any): Promise<any> {

        return new Promise(async (acc, rej) => {
            let data = param;
            let err: Error;
            let connection;
            [err, connection] = await to(DbConnection.getConnection());
            if (err) {
                return rej(getError(err));
            }
            let result: any;

            let query = `call GetProjectUser(?)`;
            var dataArr = [data.projectId];
            
           
            [err, result] = await to(connection.query(query, dataArr));
            if (err) {
                return rej(getError(err));
            }
            if (result) {
                console.log(result);
                this.response = {
                    ResponseCode: ResponseCode.Ok,
                    ResponseMessage: "",
                    ResponseData: result,
                }
                return acc(this.response);
            }
            
        }).catch((err) => {
            return Promise.reject(err);
        });
    }
    async getDashboardData(param: any): Promise<any> {
        return new Promise(async (acc, rej) => {
            let data = param;
            let err: Error;
            let connection;
            [err, connection] = await to(DbConnection.getConnection());
            if (err) {
                return rej(getError(err));
            }
            let result: any;

            let query = `call GetDashboardData(?)`;
            var dataArr = [data.userId];
            
            [err, result] = await to(connection.query(query, dataArr));
            if (err) {
                return rej(getError(err));
            }
            if (result) {
                //console.log(result);
                let resultTemplate:any={};
                resultTemplate.template = result[0];
                resultTemplate.projects = result[1];
                this.response = {
                    ResponseCode: ResponseCode.Ok,
                    ResponseMessage: "",
                    ResponseData: resultTemplate,
                }
                return acc(this.response);
            }
            
        }).catch((err) => {
            return Promise.reject(err);
        });
    }
}