export class UserModel {
    id: number;
    name: string;
    email: string;
    role: any;
    password: string;
    verification_token: any;
    org_id: any;
    company:any
    title:any
    profile_image:any
    bio:any
    is_active: boolean = true;
    is_deleted: boolean = false;
    constructor(param: any) {
        Object.assign(this, param);
    }
}