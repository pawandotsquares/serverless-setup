export class ProjectModel {
    id: number = 0;
    projectOwner: number = 0;
    organization: number = 0;
    projectType: number = 0;
    projectName: string = '';
    teamLeader: string = '';
    currentPeriod: Date = new Date;
    group: number = 0;
    projectActive: number = 1;
    is_deleted : number = 0;
    created_at : any;
    updated_at : any;
    templateId:number = 0;
    constructor(param: any) {
        Object.assign(this, param);
    }
}