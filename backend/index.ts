
import { DbConnection } from "./src/util/DbConnection";

import { TResponse, ResponseCode, MethodKeys } from "./src/util/TResponse";
import { UserBusiness } from "./src/Business/UserBusiness";
import { ProjectBusiness } from "./src/Business/ProjectBusiness";
import * as dotenv from "dotenv";
import { Handler, Context, Callback } from "aws-lambda";
import { to, EmailSend, getTokenData, authorize } from "./src/util/helper";


function parseBody(event: any) {
  dotenv.config();
  console.log("event: init");
  let data: any;
  if (event.httpMethod && event.httpMethod.toLowerCase() == "get") {
    data = event.queryStringParameters;
  } else {
    if (event.body !== null && event.body !== undefined) {
      let body: any;
      try {
        body = JSON.parse(event.body);
      } catch (e) {
        body = event.body;
      }
      data = body;
    }
  }

  return data;
}
function renderResponse(err, res, callback) {
  let response: TResponse;
  DbConnection.disposeConnection();
  if (err) {
    console.log("Error", err);
    response = {
      ResponseCode: err.code || ResponseCode.Error,
      ResponseMessage: err.message
    };
  } else {
    response = res;
  }
  return callback(null, {
    headers: {
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Credentials": false,
      // "Access-Control-Allow-Methods": "*"
    },
    body: JSON.stringify(response)
  });
}
const userLogin: Handler = (event: any, context: Context, callback: Callback) => {
  (async () => {
    let err = [];
    let result = [];
    
     [err, result] = await to(new UserBusiness().userLogin(parseBody(event)));
    renderResponse(err, result, callback);
  })();
}
const forgotPassword: Handler = (event: any, context: Context, callback: Callback) => {
  (async () => {
    console.log("test");
    let err = [];
    let result = [];
    //[err, result] = await to(new UserBusiness().forgotPassword(parseBody(event)));
    renderResponse(err, result, callback);
  })();
}
const resetPassword: Handler = (event: any, context: Context, callback: Callback) => {
  (async () => {
    let err = [];
    let result = [];
    [err, result] = await to(new UserBusiness().resetPassword(parseBody(event)));
    renderResponse(err, result, callback);
  })();
}
const changePassword: Handler = (event: any, context: Context, callback: Callback) => {
  (async () => {
    let err = [];
    let result = [];
    [err, result] = await to(authorize(event));
    if(err) {
      renderResponse(err, result, callback);
    }
    [err, result] = await to(new UserBusiness().changePassword(parseBody(event)));
    renderResponse(err, result, callback);
  })();
}
const addProject: Handler = (event: any, context: Context, callback: Callback) => {
  (async () => {
    let err = [];
    let result = [];
    [err, result] = await to(authorize(event));
    if(err) {
      renderResponse(err, result, callback);
    }
    [err, result] = await to(new ProjectBusiness().addProject(parseBody(event)));
    renderResponse(err, result, callback);
  })();
}
const updateProjectUserStatus: Handler = (event: any, context: Context, callback: Callback) => {
  (async () => {
    let err = [];
    let result = [];
    [err, result] = await to(authorize(event));
    if(err) {
      renderResponse(err, result, callback);
    }
    [err, result] = await to(new ProjectBusiness().updateProjectUserStatus(parseBody(event)));
    renderResponse(err, result, callback);
  })();
}
const getProjectUsers: Handler = (event: any, context: Context, callback: Callback) => {
  (async () => {
    let err = [];
    let result = [];
    [err, result] = await to(authorize(event));
    if(err) {
      renderResponse(err, result, callback);
    }
    [err, result] = await to(new ProjectBusiness().getProjectUsers(parseBody(event)));
    renderResponse(err, result, callback);
  })();
}
const getDashboardData: Handler = (event: any, context: Context, callback: Callback) => {
  (async () => {
    let err = [];
    let result = [];
    [err, result] = await to(authorize(event));
    if(err) {
      renderResponse(err, result, callback);
    }
    [err, result] = await to(new ProjectBusiness().getDashboardData(parseBody(event)));
    renderResponse(err, result, callback);
  })();
}
const getUserByRole: Handler = (event: any, context: Context, callback: Callback) => {
  (async () => {
    let err = [];
    let result = [];
    [err, result] = await to(authorize(event));
    if(err) {
      renderResponse(err, result, callback);
    }
    [err, result] = await to(new UserBusiness().getUserByRole(parseBody(event)));
    renderResponse(err, result, callback);
  })();
}
const getUserRoles: Handler = (event: any, context: Context, callback: Callback) => {
  (async () => {
    let err = [];
    let result = [];
    [err, result] = await to(authorize(event));
    if(err) {
      renderResponse(err, result, callback);
    }
    [err, result] = await to(new UserBusiness().getUserRoles(parseBody(event)));
    renderResponse(err, result, callback);
  })();
}
export { userLogin,forgotPassword,resetPassword,changePassword,addProject,updateProjectUserStatus,getProjectUsers,getDashboardData,getUserByRole,getUserRoles};