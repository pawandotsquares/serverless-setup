import { Component } from '@angular/core';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Adelante';
  isLoggedIn:any

  ngOnInit() {

    this.isLoggedIn = localStorage.getItem('isLoggedIn');
    let userId = localStorage.getItem('userId');
    if (this.isLoggedIn == 'true'){
      this.isLoggedIn = 'true';
    }else{
      this.isLoggedIn = 'false';
    }


  }

}
