import { environment } from 'src/environments/environment.prod';
import { decrypt } from './common';


export function to(promise) {
  return promise
    .then(data => {
      return [null, data];
    })
    .catch(err => [err]);
}

export function isValueExist(param): boolean {
  if (param) {
    if (param != undefined && param != null) {
      if (typeof param == "string") {
        if (param.length > 0) {
          return true;
        } else {
          return false;
        }
      } else if (param instanceof Array) {
        if (param.length > 0) {
          return true;
        } else {
          return false;
        }
      } else {
        return true;
      }
    }
    return false;
  }
  return false;
}

export function checkNested(obj: any, params: string) {
  let args = params.split(".");
  for (var i = 0; i < args.length; i++) {
    if (!obj || !obj.hasOwnProperty(args[i])) {
      return false;
    }
    obj = obj[args[i]];
  }
  return true;
}

export function chunk(arr, size) {
  return arr.reduce(
    (acc, _, i) => (i % size ? acc : [...acc, arr.slice(i, i + size)]),
    []
  );
}

export async function getBase64ImageFromUrl(imageUrl) {
  var res = await fetch(imageUrl);
  var blob = await res.blob();

  return new Promise((resolve, reject) => {
    var reader = new FileReader();
    reader.addEventListener(
      "load",
      function () {
        resolve(reader.result);
      },
      false
    );

    reader.onerror = () => {
      return reject(this);
    };
    reader.readAsDataURL(blob);
  });

  environment
}

export function getCurrentUser() {

  let data = localStorage.getItem(environment.env + 'currentUser');
  if (data) {
      return (decrypt(data));
  }
  return null;
}