import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { GrowlerComponent, OverlayComponent } from './components/growler/growler.component';
import { NavBarComponent } from './components/nav-bar/nav-bar.component';
import { LoginComponent } from './components/login/login.component';
import { HeaderComponent } from './components/header/header.component';
import { ForgotPasswordComponent } from './components/forgot-password/forgot-password.component';
import { ProjectComponent } from './components/project/project.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ResetpasswordComponent } from './components/resetpassword/resetpassword.component';
import { CommonModule } from "@angular/common";
import {ProjectSettingsComponent} from './components/project-settings/project-settings.component';
import { SfpProjectComponent } from './components/sfp-project/sfp-project.component';
import { MyProfileComponent } from './components/my-profile/my-profile.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { ClientsComponent } from './components/clients/clients.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxTrimDirectiveModule } from "ngx-trim-directive";


import * as _moment from 'moment';
import { ApplicationSettingsComponent } from './components/application-settings/application-settings.component';
import { ProjectEditComponent } from './components/project-edit/project-edit.component';
import { SpinnerComponent } from './components/spinner/spinner.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    GrowlerComponent,
    NavBarComponent,
    LoginComponent,
    HeaderComponent,
    ForgotPasswordComponent,
    ProjectComponent,
    ResetpasswordComponent,
    ProjectSettingsComponent,
    SfpProjectComponent,
    ClientsComponent,
    MyProfileComponent,
    ApplicationSettingsComponent,
    ProjectEditComponent,
    SpinnerComponent,
    OverlayComponent
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    CommonModule,
    NgSelectModule,
    FormsModule,
    NgxTrimDirectiveModule,
    NgbModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
