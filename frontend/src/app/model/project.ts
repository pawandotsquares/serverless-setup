
export interface Project {
    
    organizationList:object;
    projectName: any
    organization:any;
    projectType:any;
    projectOwner:any;
    ownerList:object;
    teamLeader:any;
    projectActive:any;
    group:any;
    currentPeriod:any;
    lastOrgId:boolean;
    isLoggedIn:any;
    userList: any;
    orgemail:any;
    orgname:string;
    projectId:Number;
}