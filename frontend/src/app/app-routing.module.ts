import { NgModule } from '@angular/core';
import { AuthGuard } from "./services/auth.guard";
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { ForgotPasswordComponent } from './components/forgot-password/forgot-password.component';
import { ResetpasswordComponent } from './components/resetpassword/resetpassword.component';
import { ProjectComponent } from './components/project/project.component';
import { SfpProjectComponent } from './components/sfp-project/sfp-project.component';
import { ProjectSettingsComponent } from './components/project-settings/project-settings.component';
import { MyProfileComponent } from './components/my-profile/my-profile.component';
import { ClientsComponent } from './components/clients/clients.component';
import { ApplicationSettingsComponent } from './components/application-settings/application-settings.component';


const routes: Routes = [
  { path: "dashboard", component: HomeComponent, canActivate: [AuthGuard] },
  { path: "forgot-password", component: ForgotPasswordComponent },
  { path: "project", component: ProjectComponent, canActivate: [AuthGuard] },
  {
    path: "sfp-project",
    component: SfpProjectComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "resetpassword",
    component: ResetpasswordComponent
  },
  {
    path: "sfp-project",
    component: SfpProjectComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "project-settings",
    component: ProjectSettingsComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "project-settings/:id",
    component: ProjectSettingsComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "my-profile",
    component: MyProfileComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "clients",
    component: ClientsComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "application-settings",
    component: ApplicationSettingsComponent,
    canActivate: [AuthGuard]
  },
  { path: "", component: LoginComponent },
  { path: "**", redirectTo: "dashboard", pathMatch: "full" }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
