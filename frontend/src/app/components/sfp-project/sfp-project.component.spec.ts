import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SfpProjectComponent } from './sfp-project.component';

describe('SfpProjectComponent', () => {
  let component: SfpProjectComponent;
  let fixture: ComponentFixture<SfpProjectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SfpProjectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SfpProjectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
