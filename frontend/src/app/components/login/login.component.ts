import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CommonService } from '../../services/commonservices';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { GrowlerService } from '../../services/growler.service';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"]
})
export class LoginComponent implements OnInit {
  private loggedIn: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(
    false
  );
  public user: any = {};
  get isLoggedIn() {
    return this.loggedIn.asObservable();
  }
  message: any;
  notValid:boolean=false;
  //loading: boolean = true;
  
  constructor(
    private router: Router,
    private commonService: CommonService,
    public growlerService: GrowlerService
  ) {}

  ngOnInit(): void {
  
    if (localStorage.getItem('isLoggedIn') == "true") {
      this.router.navigateByUrl("/dashboard");
    }

  }

  onSubmit() {
      //this.growlerService.success("Login Sucessfully");
      //this.growlerService.clear();
    this.commonService.userlogin(this.user).subscribe(loginResult => {
      //this.loading = true;
       if (loginResult.statusCode == 200) {
          this.loggedIn.next(true);
          this.message = loginResult.message;
          localStorage.setItem("isLoggedIn", "true");
          localStorage.setItem("user_session_data", loginResult);
          localStorage.setItem("token", loginResult.data.verification_token);
          localStorage.setItem("userId", loginResult.data.id);
          localStorage.setItem("email", this.user.email);
          localStorage.setItem("role", loginResult.data.role);
          localStorage.setItem("username", loginResult.data.user_name);
          
          this.router.navigateByUrl("/dashboard");
          this.growlerService.success("Login Sucessfully");
          this.growlerService.clear();
          //this.loading = false;
        } else {
          //this.loading = false;
          this.notValid =true
          setTimeout(()=>{  
            this.notValid =false
          }, 4000);
          this.message = loginResult.message;
          
          //this.message="The e-mail address and/or password do not match our records. Please try again";
          localStorage.setItem("statusCode", loginResult.statusCode);
          this.growlerService.error("Login Failed");
          this.growlerService.clear();
        }
      });
   }

}
