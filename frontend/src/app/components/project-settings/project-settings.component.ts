  import { Component, OnInit, ViewChild } from "@angular/core";
  import { CommonService } from '../../services/commonservices';
  import { ActivatedRoute, Router } from '@angular/router';
  import { FormGroup, Validators } from '@angular/forms';
  import { GrowlerService } from '../../services/growler.service';
  import { Project } from '../../model/project';
  import { Organization } from '../../model/organization';
  import { Adduserorganization } from '../../model/adduserorganization';
  import { NgbDate } from '@ng-bootstrap/ng-bootstrap';

  @Component({
    selector: "app-project-settings",
    templateUrl: "./project-settings.component.html",
    styleUrls: ["./project-settings.component.scss"]
  })
  export class ProjectSettingsComponent implements OnInit {
    @ViewChild("forguser") forguser; // Added this
    @ViewChild('closebutton') closebutton;
    projectsettingForm: FormGroup;
    organizationList: object;
    roleList: object;
    ownerList: object;
    projectUserList:object;
    lastOrgId: boolean;
    isLoggedIn: any;
    organizationUserForm: FormGroup;
    organizationForm: FormGroup;
    submitted: boolean = false;
    notSelected: boolean = false;
    IsHidden: boolean = true;
    OrgUsersubmitted = false;
    afterSave: boolean = false;
    IsmodelShow:boolean =false;
    authtoken: any;
    reasonValue: object;
    DropdownVar: any;
    projectsubmitted = false;
    organizationUserusubmitted = false;
    organizationsubmitted = false;
    userlist: any = [];
    ownerArray: any =[];
    userlistArray: any = [];
    searchUsers: any = [];
    date: any;
    pType: any;
    gType: any;
    modalVisible = false;
    loading: boolean = false;
    projectIdNotExist:boolean =false;
    createProjectID:any;
    display='none'; 

    
    public Project: Project = {} as Project;
    public Organization: Organization = {} as Organization;
    public Adduserorganization: Adduserorganization = {} as Adduserorganization;
    constructor(
      private commonService: CommonService,
      private route: ActivatedRoute,
      private router: Router,
      public growlerService: GrowlerService
    ) {}

    selectedUser: any;
    selectedUserName = "";
    selectedUserId: number;
    selectedUserIds: number[];
    public destinations = [];

    ngOnInit(): void {
      this.Project = {} as Project;
      this.Organization = {} as Organization;
      this.Adduserorganization = {} as Adduserorganization;
      this.DropdownVar = "";
      this.pType = localStorage.getItem("pType");
      this.gType = localStorage.getItem("gType");
      this.Project.projectOwner = null;

      this.isLoggedIn = localStorage.getItem("isLoggedIn");
      this.authtoken = localStorage.getItem("isLoggedIn");

      if (this.isLoggedIn == "true") {
          this.getOrganizationList();
      }
      if (this.pType && this.gType) {
          this.projectFromTemplate();
      } else {
          this.projectFromScratch();
      }
      //this.loading = false;
    }

    projectFromScratch() {
      this.Project.group = null;
      this.Project.projectType = null;
      console.log("group");
      console.log(this.Project.group);
    }

    projectFromTemplate() {
      this.Project.group = parseInt(this.gType);
      this.Project.projectType = parseInt(this.pType);
    }

    getOrganizationList() {
      let data = {
        orgId: "",
        role: 4
      };
      this.commonService
        .GetOwnerByOrganizationList(data)
        .subscribe(organizationResult => {
          if (organizationResult.statusCode == 200) {
            this.organizationList = organizationResult.data;
            this.getUserRoleList();
          } else {
            //this.message = resultReasonData.message;
          }
        });
    }

    getUserRoleList() {
      let data = {};
      this.commonService.GetUserRoleList(data).subscribe(roleResult => {
        if (roleResult.statusCode == 200) {
          this.roleList = roleResult.data;
          //this.getOrganizationList()
        } else {
          //this.message = resultReasonData.message;
        }
      });
    }

    // adduser(){
    //   console.log(this.userlist);
    //   console.log('sbi2');
    //   const listArray = Object.assign([],this.userlist);
    //   this.userlistArray.push(listArray);
    //   //this.userlist={}
    // }

    // get formsub() {
    //   return this.organizationForm.controls;
    // }

    adduser(userlistParam: any = []) {
      this.userlistArray = userlistParam;
    }

    get formsaveproject() {
      return this.projectsettingForm.controls;
    }

    onAddNewUserDirect() {
      this.DropdownVar = 2;
    }
    
    onProjectSubmit(f: any) {
      // console.log('looooo');
      // return true;
      let self = this;
      if (!this.Project.projectActive) {
        this.Project.projectActive = false;
      }
      if (!this.Project.teamLeader) {
        this.Project.teamLeader = "";
      }
      //this.Project.currentPeriod = new Date().toLocaleDateString();
      this.Project.currentPeriod = new Date().toISOString().slice(0,10);
      let cDate = this.Project
      console.log(self);
      console.log('PROJECTDARA')
     
      self.commonService.createProject(self.Project).subscribe(
        result => {
          console.log(result);
          console.log('PROJECT');
          if (result.statusCode == 200) {
            this.projectIdNotExist = true;
            this.Project.projectId = result.lastId;
            localStorage.removeItem("pType");
            localStorage.removeItem("gType");
            this.growlerService.success("Project successfully added.");
            this.growlerService.clear();
            if (this.Project.projectId) {
              this.GetUserListingProjectwise(result.lastId);
            }
          }
        },
        error => {}
      );
      this.projectsubmitted = true;
      // if (this.projectsettingForm.invalid) {
      //   return;
      // }
    }

    GetUserListingProjectwise(projectId: number) {
      this.commonService
        .GetUserListingProjectwise(projectId)
        .subscribe(projectwiseUserListResult => {
          this.afterSave = true;
        });
    }

    onAddUser(formUser: any) {
      this.userlist = formUser.value;
      const listArray = Object.assign({}, this.userlist);
      this.userlistArray.push(listArray);
      //console.log(this.userlistArray);
      console.log('kljkklk');
      this.adduser(this.userlistArray);
      this.forguser.resetForm();
      this.display='none';
      //this.Adduserorganization = {} as Adduserorganization;

      if (this.DropdownVar == 2) {
        let selfuser = this;
        let data = {
          projectId:this.Project.projectId,
          //projectId: 1,
          selectedUserId: "",
          User: selfuser.userlist
        };

        console.log(data);
        console.log('project assign data');
        //this.closebutton.nativeElement.click();
        this.commonService
          .addUserInProject(data)
          .subscribe(addUserInProjectResult => {
            console.log('apis');
            console.log(addUserInProjectResult);
            console.log(addUserInProjectResult);
          }); 
      }
    }

    deleteUser(e: any) {
      this.userlistArray.splice(0,1);
    }

    onClose(isVisible: boolean){
      this.modalVisible = isVisible;
     }

    selectedUserAdd() {
      console.log("selecteduser");
      console.log(this.selectedUser);
      if(!this.selectedUser){
        this.notSelected=true;
        setTimeout(()=>{
          this.notSelected=false;
        },4000);
        
      }else{
        this.notSelected=false;
      }

      if(this.Project.projectId){
          this.createProjectID = this.Project.projectId
      }else{
        this.notSelected=false;
        this.projectIdNotExist = true;
      }

     
      //let selectedUserId=this.selectedUser;
      if(this.selectedUser){
        let data = {
          organizationId:0,
          projectId: this.createProjectID,
          selectedUserId: this.selectedUser,
          roleId:2
        };
        this.commonService
          .addUserInProject(data)
          .subscribe(addUserInProjectResult => {
            if (addUserInProjectResult.statusCode == 200) {
              this.projectUserList=addUserInProjectResult.data;
            }
  
            console.log(addUserInProjectResult);
          });
      }
      
    }

    onorganizationSubmit(forg: any){
      
      this.organizationsubmitted = true;
      let data = {
        org_id: 0,
        name: this.Organization.orgname,
        email: this.Organization.orgemail,
        role: 4,
        users: this.userlistArray
      };
      console.log(this.userlistArray.length);
      if(this.Organization.orgname && (this.userlistArray.length !=0)){
        this.commonService
        .addOrganization(data)
        .subscribe(addOrganizationResult => {
          if (addOrganizationResult.statusCode == 200) {
            
            this.organizationList = addOrganizationResult.data;
            this.lastOrgId = addOrganizationResult.lastId;
            let getOwner = {
              orgId: this.lastOrgId,
              role: 0
            };

            document.getElementById("closeAddOrgUserPopup").click();
            this.commonService
              .GetOwnerByOrganizationList(getOwner)
              .subscribe(getOwnerbyOrganizationResult => {
                if (getOwnerbyOrganizationResult.statusCode == 200) {
                  this.ownerList=getOwnerbyOrganizationResult.data;
                  localStorage.removeItem("pType");
                  localStorage.removeItem("gType");
                    this.growlerService.success("Successfully added organization.");
                    this.growlerService.clear();
                    this.ownerArray =this.ownerList;
                    this.searchUsers = this.ownerList;
                    if(this.ownerList){
                        this.Project.projectOwner = this.ownerList[this.ownerArray.length-1]['id'];  
                    }
              }else{

              }

              });

          } else if (addOrganizationResult.statusCode == 409) {
            this.growlerService.error(addOrganizationResult.message);
            this.growlerService.clear();
          } else {
            console.log("Noentry");
          }
        });
      }else{
        
        this.growlerService.error("Please add  atlest one user in this organization.");
        this.growlerService.clear();
      }
      //return true;
      

      //this.growlerService.success("This adds a success message");
      // }else{
      //   console.log("Please enter all options");
      // }
    }

    getorgNameText(orgTxt: any) {
      console.log(orgTxt);
    }

    changeOrganization(org) {
      //console.log('change orga');
      let selectedOrgId = org.value;
      let getOwner = {
        orgId: selectedOrgId,
        role: 0
      };
      //console.log(getOwner);
      this.loading = true;
      this.commonService
        .GetOwnerByOrganizationList(getOwner)
        .subscribe(getownerbyOrganizationResult => {
          this.loading = false;
          //this.loading = false;
         // this.ownerList = {};  
         // this.projectUserList = {}; 

          if(getownerbyOrganizationResult.statusCode == 200){
           
              this.ownerList = getownerbyOrganizationResult.data;
              this.searchUsers = this.ownerList;
            
          }          
        
          
        });
        
    }
    
    confirm() {
      this.IsmodelShow=true;
      setTimeout(()=>{
        this.router.navigateByUrl("/dashboard");
      },1000)
      
    }


    assignProjectToUser(e) {
      if(e.target.checked){     
        console.log('in');   
      }else{
        console.log('outin');   
      }
   }

  }
