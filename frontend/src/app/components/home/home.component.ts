import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../services/commonservices';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { GrowlerService } from '../../services/growler.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  public sectionToggle1: boolean = true;
  public sectionToggle2: boolean = true;
  public showLoder: boolean = true;
  
  activeProjectList:any = [];
  templatesList:object;
  pType:number;
  gType:number;
  createdprojectId:number;
  //getActiveProjectDashboard:object;


  isLoggedIn:any
  constructor(
    private commonService: CommonService,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    public growlerService : GrowlerService,
    public router: Router
  ) { }

  ngOnInit(): void {
    this.showLoder = true
    let self= this
    let token='';
    setTimeout(()=>{  
        this.showLoder =false
        let token = localStorage.getItem('token')
        this.isLoggedIn = localStorage.getItem('isLoggedIn');
        let userId = localStorage.getItem('userId');
        if (this.isLoggedIn == 'true' && token){
          this.getActiveProjectDashboard(userId)
        }
      }, 2000);
    
    localStorage.removeItem("pType");
    localStorage.removeItem("gType");
    //this.isLoggedIn = 'true';
  }

  projectType(gType,pType){
    console.log('herll');
    localStorage.setItem("pType",pType);
    localStorage.setItem("gType",gType);
    this.router.navigateByUrl("/project-settings");

  }

  getActiveProjectDashboard(userId){
    //let userIds='59';
    this.commonService.GetActiveProjectDashboard(userId).subscribe(activeProjectResult => {
      if (activeProjectResult.statusCode == 200) {
        this.activeProjectList = activeProjectResult.project;
        this.templatesList = activeProjectResult.templates;

      }else {
        //this.message = resultReasonData.message;
      }
  })



  }



}
