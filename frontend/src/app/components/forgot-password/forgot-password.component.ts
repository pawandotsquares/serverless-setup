import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../services/commonservices';
import { GrowlerService } from '../../services/growler.service';
import { Router, ActivatedRoute } from '@angular/router';
@Component({
  selector: "app-forgot-password",
  templateUrl: "./forgot-password.component.html",
  styleUrls: ["./forgot-password.component.scss"]
})
export class ForgotPasswordComponent implements OnInit {
  message: any;
  public user: any = {};  email: any;
  notValid:boolean=false;
  validTrue:boolean=false;
  constructor(
    private commonService: CommonService,
    public growlerService: GrowlerService,
    private router: Router
  ) {}

  ngOnInit(): void {

  }

  onSubmit() {
    let data = {
      email: this.user.email
    };
    this.commonService.forgotPass(data).subscribe(
      result => {
        if (result.statusCode == 200) {
          //this.message = result;
          this.validTrue =true
          setTimeout(()=>{  
            this.validTrue =false
          }, 4000);
          this.message = result.message;
          //this.growlerService.success(result.message);
          //this.growlerService.clear();
          setTimeout(()=>{  
            this.router.navigateByUrl('/');
          }, 4000);
          
        } else {
          this.notValid =true
          setTimeout(()=>{  
            this.notValid =false
          }, 4000);
          this.message = result.message;
          //this.message = result;
          this.growlerService.error(result.message);
          this.growlerService.clear();
        }
      },
      error => {}
    );
  }
}
