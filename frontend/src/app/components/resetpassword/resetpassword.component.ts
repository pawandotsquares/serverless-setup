import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { CommonService } from '../../services/commonservices';
import { GrowlerService } from '../../services/growler.service';

@Component({
  selector: "app-resetpassword",
  templateUrl: "./resetpassword.component.html",
  styleUrls: ["./resetpassword.component.scss"]
})
export class ResetpasswordComponent implements OnInit {
  message: any;
  token: any;
  public user: any = {};
  htmlOptions: any = {};
  notValid:boolean=false;
  validTrue:boolean=false;
  constructor(
    private commonService: CommonService,
    public growlerService: GrowlerService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.route.queryParamMap.subscribe(params => {
      this.token = params.get("token");
    });
  }

  // convenience getter for easy access to form fields

  onSubmit() {
    if (this.user.password != this.htmlOptions.confirm_password) {
      return;
    }
    let data = {
      token: this.token,
      password: this.user.password
    };

    //call login api here
    this.commonService.resetPass(data).subscribe(
      result => {
        if (result.statusCode == 200) {
          //this.message = result;

          this.validTrue =true
          setTimeout(()=>{  
            this.validTrue =false
          }, 4000);
          this.message = result.message;

          
          setTimeout(()=>{  
            this.router.navigateByUrl('/');
          }, 4000);
        } else {
          this.notValid =true
          setTimeout(()=>{  
            this.notValid =false
          }, 4000);
          this.message = result.message;

          //this.message = result;
          //this.growlerService.error(result.message);
          //this.growlerService.clear();
          localStorage.setItem('statusCode', result.statusCode);
        }
      },
      error => {}
    );
  }
}
