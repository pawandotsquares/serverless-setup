import { Component, OnInit } from '@angular/core';
import { GrowlerService} from 'src/app/services';
//import { AuthService } from '../../services/auth.service';
import { Router, ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.scss']
})
export class NavBarComponent implements OnInit {
  constructor(public growlerService: GrowlerService) { }
  isLoggedIn:any
  ngOnInit(): void {
    this.isLoggedIn = localStorage.getItem('isLoggedIn');

    if (this.isLoggedIn == 'true'){
      this.isLoggedIn = 'true';
    }else{
      this.isLoggedIn = 'false';
    }
    
  }

  

}
