import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../auth/auth.service';
import { Observable } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  isLoggedIn:any
  username:string
  useremail:any
  constructor(
    private authService: AuthService,
    public router: Router
    ) { }

  ngOnInit(): void {
    this.isLoggedIn = localStorage.getItem('isLoggedIn');
    if (this.isLoggedIn == 'true'){
      this.isLoggedIn = 'true';
      this.username = localStorage.getItem('username');
      this.useremail = localStorage.getItem('email');
    }else{
      this.isLoggedIn = 'false';
    }
  }

  onLogout() {
    this.authService.logout();
    this.router.navigate(['']);
  }

}
