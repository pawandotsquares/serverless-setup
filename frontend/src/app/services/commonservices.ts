import { Injectable } from "@angular/core";
import { HttpClient, HttpParams, HttpErrorResponse } from "@angular/common/http";
import { HttpHeaders } from "@angular/common/http";
import { environment } from "../../environments/environment";
import { Observable, throwError } from "rxjs";
import { map, take, retryWhen, catchError } from 'rxjs/operators';
import { forkJoin } from 'rxjs';

let authtoken ="";
//let isLoggedIn = "";
//setTimeout(()=>{
let isLoggedIn = localStorage.getItem('isLoggedIn');
//},2000);

if(isLoggedIn == "true"){
 authtoken = localStorage.getItem('token');
}

const httpOptions = {
  headers: new HttpHeaders({
    "Content-Type": "application/json",
    "Access-Control-Allow-Methods": "*",
    "Access-Control-Allow-Headers":"*",
    "Authorization":authtoken
  }),

};


@Injectable({
  providedIn: "root"
})

//Calling To Service Endpoints
export class CommonService {
  public API_URL = environment.API_BASE_URL
  request: any = {};
  constructor(private http: HttpClient) { }

  userlogin(param: any): Observable<any> {
    //this.request['data'] = param
    //console.log(this.API_URL);
    return this.http.post<any>(this.API_URL + "/UserLogin", param, httpOptions);
  }

  forgotPass(param: any): Observable<any> {

   return this.http.post<any>(
     this.API_URL + "/ForgotPassword",
     param,
     httpOptions
   );
  }

  resetPass(param: any): Observable<any> {
   return this.http.post<any>(this.API_URL+'/resetPassword',param,httpOptions)
  }

  GetOwnerByOrganizationList(param: any): Observable<any> {
   return this.http.get(this.API_URL+`/users/byrole?orgId=`+param.orgId+`&role=`+param.role,httpOptions);
  }


  // getOwnerbyOrganizationResult(param: any): Observable<any> {
  //   return this.http.get(this.API_URL+`/users/byrole?orgId=`+param.orgId+`&role=`+param.role,httpOptions);
  //  }

  GetUserRoleList(param: any): Observable<any> {
    return this.http.get<any>(this.API_URL+'/users/roles',httpOptions)
   }

  GetUserListingProjectwise(param: any): Observable<any> {
    return this.http.get<any>(this.API_URL+`/projects/getUsers?projectId=`+param,httpOptions)
   }


  addOrganization(param:any): Observable<any> {
    return this.http.post<any>(this.API_URL+'/users/add',param,httpOptions)
  }

  createProject(Post: any): Observable<any> {
    return this.http.post(this.API_URL + `/projects/add`, Post, httpOptions,
    ).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }

  GetActiveProjectDashboard(param:any): Observable<any> {
    return this.http.get<any>(this.API_URL+`/projects/userDashbordData?userId=`+param,httpOptions)

  }


  // addUserInProject(param:any): Observable<any> {
  //   //console.log(param);
  //   return this.http.post<any>(this.API_URL+'/users/add',param,httpOptions)

  // }

  addUserInProject(param:any): Observable<any> {
    //console.log(param);
    return this.http.post<any>(this.API_URL+'/projects/addUser',param,httpOptions)

  }
  // getOwnerbyOrgId(param: any): Observable<any> {
  //   return this.http.get(this.API_URL+`/users/byrole?orgId=`+param.orgId+`&role=`+param.role,httpOptions);
  // }


}



