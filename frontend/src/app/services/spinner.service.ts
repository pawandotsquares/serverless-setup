import { Injectable, TemplateRef, ViewContainerRef } from '@angular/core';
import { Overlay, OverlayConfig, OverlayRef, PositionStrategy } from '@angular/cdk/overlay';
import { ComponentPortal } from '@angular/cdk/portal';
import {SpinnerComponent} from '../components/spinner/spinner.component';

@Injectable({
  providedIn: 'root'
})
export class SpinnerService {
  overlayRef: any;
  constructor(private overlay: Overlay) {
    
   }
  
 loaderOverlay() {
  this.overlayRef = this.overlay.create({
    positionStrategy: this.overlay.position().global().centerHorizontally().centerVertically(),
    hasBackdrop: true
  });
  this.overlayRef.attach(new ComponentPortal(SpinnerComponent));
 }  

 detachOverlay(){
  this.overlayRef.detach()
 }
  
}
